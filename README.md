# Booking Mask - EveryMundo

🌎 Try demo at: [index.html](https://bitbucket.org/ivansandoya/everymundo/src/master/index.js)

![alt text](https://bitbucket.org/ivansandoya/everymundo/raw/e0bad86078c17522c76445c6c8b65574c9142ae0/src/assets/capture.png)

## Development procedure:
###1. Configure webpack for a React project
Webpack is one of the most popular modern bundlers for JavaScript. I created two WP configuration
files, one for development and the other one for production. Inside webpack loaders for CSS styling, images and babel loading were used.

###2. Init project and install all pre required dev dependencies
Dependencies for babel support, testing and also React presets were installed. 

###3. Identify React components and design Redux store
Nothing better than a sketch to figure out what the components and also Redux store will be.
![alt text](https://bitbucket.org/ivansandoya/everymundo/raw/e0bad86078c17522c76445c6c8b65574c9142ae0/src/assets/sketch.jpeg)

###4. Set project directories structure
As seen in this repository the project consists of one entry point (index.js) which has the initial 
Redux configuration and also it calls the Booking component that is wrapped inside a high order component (Redux Provider).
All the developed files were placed inside _src/_ dir (this includes actions, reducers and tests)

###5. Develop components, actions and reducers
Components were clasified in smart and functional ones. There are directories for layout components (flight/) and user interface ones (ui/).
All reducers and actions were stored in their correspondent folders.

###6. Develop and run unit tests
I decided to use **_Jest_** library for React unit testing. This utility requires of **_enzyme_** package which allows us to simulate components, their DOM manipulation and also their behaviour.
An example of the **_PlaceSelector_** test component was implemented.
![alt text](https://bitbucket.org/ivansandoya/everymundo/raw/e0bad86078c17522c76445c6c8b65574c9142ae0/src/assets/test.png)

### 👨🏻‍💻 Note from the Developer:
In order to expedite the demonstration of this practical exercise, some details like deep CSS styling
or a higher test coverage were overlooked.
