import { combineReducers } from 'redux'
import placesuggestion from './placesuggestion'
import { reducer as formReducer } from 'redux-form'

// return all combine reducers
const rootReducer = combineReducers({
    placesuggestion,
    form: formReducer
})

export default rootReducer
