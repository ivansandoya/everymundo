import {
    FETCH_REQUESTED,
    CLEAR_REQUESTED
} from '../actions/placesuggestion'

const initialState = {
    suggestions: []
}

export default function placeSuggestionReducer(state = initialState, action) {
    switch(action.type) {
        case FETCH_REQUESTED:
            // load places suggestions
            return {
                ...state,
                suggestions: action.payload
            }

        case CLEAR_REQUESTED:
            // clear all places suggestions
            return {
                ...state,
                suggestions: []
            }

        default:
            return state
    }
}
