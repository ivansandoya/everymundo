import React from 'react'
import PlaceSelector from '../ui/placeselector/components/PlaceSelector'
import { shallow } from 'enzyme'

describe('PlaceSelector', () => {
    const wrapper = shallow(<PlaceSelector 
        meta={{active: false}}
        input={{value: ''}}
        suggestions={[]}
    />)

    it('has a p element rendered proppertly', () => { 
        expect(wrapper.find('p').exists());
    })

    it('has an Autosuggest component rendered proppertly', () => { 
        expect(wrapper.find('Autosuggest').exists());
    })

    it('has an input element rendered proppertly', () => { 
        expect(wrapper.find('input').exists());
    })
})
