import React from 'react'
import PlaceSelector from '../../ui/placeselector/components/PlaceSelector'
import DatePicker from '../../ui/datepicker/components/DatePicker'
import PassengerSelector from '../../ui/passengerselector/components/PassengerSelector'
import Button from '../../ui/button/components/Button'
import { reduxForm, Field } from 'redux-form'
import './flight.css'

function FlightLayout (props) {
    return (
        <div className="flight">
            <div className="row">
                <div className="fromTo">
                    <div className="w50">
                        <Field 
                            name={'from'}
                            component={PlaceSelector}
                            placeholder='From'
                        />
                        <svg viewBox="0 0 48 48" xmlns="http://www.w3.org/2000/svg"><path d="M20.36 18"/><path d="M42 32v-4l-16-10v-11c0-1.66-1.34-3-3-3s-3 1.34-3 3v11l-16 10v4l16-5v11l-4 3v3l7-2 7 2v-3l-4-3v-11l16 5z"/><path d="M0 0h48v48h-48z" fill="none"/></svg>
                    </div>
                    <div className="w50 toPlace">
                        <Field 
                            name={'to'}
                            component={PlaceSelector}
                            placeholder='To'
                        />
                    </div>
                    <div className="clear"></div>
                </div>
                <div className="dates">
                    <div className="w50">
                        <Field 
                            name={'departure'}
                            component={DatePicker}
                            placeholder='Departure Flight'
                        />
                    </div>
                    <div className="w50 returnDate">
                        <Field 
                            name={'return'}
                            component={DatePicker}
                            placeholder='Return Flight'
                        />
                    </div>
                    <div className="clear"></div>
                </div>
                <div className="passengers">
                    <PassengerSelector />
                </div>
                <div className="clear"></div>
            </div>
            <div className="row bottom">
                <div className="links">
                    <a href="#">Advanced search &gt;&gt;</a>
                    <a href="#">Arrivals and departures &gt;&gt;</a>
                    <a href="#">Inspire me &gt;&gt;</a>
                    <a href="#">Miles & more &gt;&gt;</a>
                    <a href="#">Check-in &gt;&gt;</a>
                </div>
                <div className="buttonWrapper">
                    <Button 
                        label="Search"
                        onClick={props.handleSubmit}
                    />
                </div>
                <div className="clear"></div>
            </div>
            <div className="clear"></div>
        </div>
    )
}

FlightLayout = reduxForm({
    // reduxForm configuration
    form: 'flight',
    initialValues: {
        adults: '1',
        children: '0',
        infants: '0'
    }
})(FlightLayout)

export default FlightLayout
