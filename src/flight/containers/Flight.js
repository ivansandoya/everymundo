import React, { Component } from 'react'
import FlightLayout from '../components/FlightLayout'

class Flight extends Component {
    submitForm = values => {
        // submit implementation, this could also be a call to an API rest
        const formValues = values

        // initial values
        const flight_type = 'Outbound'
        let origin = ''
        let destination = ''
        let departure_date = ''
        let return_date = ''
        const adult_amount = formValues.adults
        const children_amount = formValues.children
        const infant_amount = formValues.infants
        const flight_class = 'class-economy'
        
        if(formValues.from) {
            origin = formValues.from.substring((formValues.from.length - 4), formValues.from.length - 1)
        }
        if(formValues.to) {
            destination = formValues.to.substring((formValues.to.length - 4), formValues.to.length - 1)
        }
        
        if(formValues.departure) {
            departure_date = formatDate(formValues.departure);
        }
        if(formValues.return) {
            return_date = formatDate(formValues.return);
        }

        let url = ''
        
        if(origin !== '' && destination !== '' && departure_date !== '' && return_date !== '') {
            // if received all values then proceed to submit form
            // this could also be replaced with reduxForm validations
            url = `https://www.swiss.com/us/en/Book/${flight_type}/${origin}-${destination}/from-${departure_date}/to-${return_date}/adults-${adult_amount}/children-${children_amount}/infants-${infant_amount}/${flight_class}/al-LX/sidmbvl`
            window.location = url
        }
    }

    render() {
        return (
            <FlightLayout 
                onSubmit={this.submitForm}
            />
        )
    }
}

function formatDate(date) {
    // helper to format date from traditional javascript format to 'yyyy-mm-dd'
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
}

export default Flight
