// simulation of api
export const places = [
    {
        name: 'Amsterdam',
        code: 'AMS'
    },
    {
        name: 'Atlanta',
        code: 'ATL'
    },
    {
        name: 'Abu Dhabi',
        code: 'AUH'
    },
    {
        name: 'Buenos Aires',
        code: 'BUE'
    },
    {
        name: 'Barcelona',
        code: 'BCN'
    },
    {
        name: 'Bangkok',
        code: 'BKK'
    },
    {
        name: 'Cairo',
        code: 'CAI'
    },
    {
        name: 'Cape Town',
        code: 'CPT'
    },
    {
        name: 'Chicago',
        code: 'CHI'
    },
    {
        name: 'Denver',
        code: 'DEN'
    },
    {
        name: 'Detroit',
        code: 'DTT'
    },
    {
        name: 'Dubai',
        code: 'DXB'
    },
    {
        name: 'Edinburgh',
        code: 'EDI'
    },
    {
        name: 'Eliat',
        code: 'ETH'
    },
    {
        name: 'East London',
        code: 'ELS'
    },
    {
        name: 'Frankfurt',
        code: 'FRA'
    },
    {
        name: 'Fort Lauderdale',
        code: 'FLL'
    },
    {
        name: 'Fuerteventura',
        code: 'FUE'
    },
    {
        name: 'Grand Cayman',
        code: 'GCM'
    },
    {
        name: 'Glasgow',
        code: 'GLA'
    },
    {
        name: 'Guayaquil',
        code: 'GYE'
    },
    {
        name: 'Hamburg',
        code: 'HAM'
    },
    {
        name: 'Havana',
        code: 'HAV'
    },
    {
        name: 'Hong Kong',
        code: 'HKG'
    },
    {
        name: 'Istanbul',
        code: 'IST'
    },
    {
        name: 'Ibiza',
        code: 'IBZ'
    },
    {
        name: 'Indianapolis',
        code: 'IND'
    },
    {
        name: 'Istanbul',
        code: 'IST'
    },
    {
        name: 'Ibiza',
        code: 'IBZ'
    },
    {
        name: 'Indianapolis',
        code: 'IND'
    },
    {
        name: 'Johannesburg',
        code: 'JNB'
    },
    {
        name: 'Jakarta',
        code: 'JKT'
    },
    {
        name: 'Jacksonville',
        code: 'JAX'
    },
    {
        name: 'Kuwuait',
        code: 'KWI'
    },
    {
        name: 'Kiev',
        code: 'IEV'
    },
    {
        name: 'Kuala Lumpur',
        code: 'KUL'
    },
    {
        name: 'London',
        code: 'LON'
    },
    {
        name: 'Los Angeles',
        code: 'LAX'
    },
    {
        name: 'Las Vegas',
        code: 'LAS'
    },
    {
        name: 'Milan',
        code: 'MIL'
    },
    {
        name: 'Madrid',
        code: 'MAD'
    },
    {
        name: 'Miami',
        code: 'MIA'
    },
    {
        name: 'New York',
        code: 'NYC'
    },
    {
        name: 'Nassau',
        code: 'NAS'
    },
    {
        name: 'Natal',
        code: 'NAT'
    },
    {
        name: 'Paris',
        code: 'PAR'
    },
    {
        name: 'Pittsburg',
        code: 'PIT'
    },
    {
        name: 'Phoenix',
        code: 'PHX'
    },
    {
        name: 'Quebec',
        code: 'YQB'
    },
    {
        name: 'Quingdao',
        code: 'TAO'
    },
    {
        name: 'Rome',
        code: 'ROM'
    },
    {
        name: 'Rio de Janeiro',
        code: 'RIO'
    },
    {
        name: 'Riga',
        code: 'RIX'
    },
    {
        name: 'San Francisco',
        code: 'SFO'
    },
    {
        name: 'São Paulo',
        code: 'SAO'
    },
    {
        name: 'Sydney',
        code: 'SYD'
    },
    {
        name: 'Tel Aviv',
        code: 'TLV'
    },
    {
        name: 'Tokio',
        code: 'TYO'
    },
    {
        name: 'Toronto',
        code: 'YTO'
    },
    {
        name: 'Umea',
        code: 'UME'
    },
    {
        name: 'Udon Thani',
        code: 'UTH'
    },
    {
        name: 'Upington',
        code: 'UTN'
    },
    {
        name: 'Venice',
        code: 'VCE'
    },
    {
        name: 'Vienna',
        code: 'VIE'
    },
    {
        name: 'Vancouver',
        code: 'YVR'
    },
    {
        name: 'Washington',
        code: 'WAS'
    },
    {
        name: 'Wellington',
        code: 'WLG'
    },
    {
        name: 'Winnipeg',
        code: 'YWG'
    },
    {
        name: 'Xian',
        code: 'SIA'
    },
    {
        name: 'Xiamen',
        code: 'XMN'
    },
    {
        name: 'Zurich',
        code: 'ZRH'
    },
    {
        name: 'Zagreb',
        code: 'ZAG'
    },
    {
        name: 'Zakynthos',
        code: 'ZTH'
    },
]