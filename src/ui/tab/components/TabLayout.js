import React from 'react'
import Flight from '../../../flight/containers/Flight'
import './tab.css'

function TabLayout (props) {
    return (
        <div className="tab">
            <div className="nav">
                <ul>
                    <li className="active">
                        <svg viewBox="0 0 48 48" xmlns="http://www.w3.org/2000/svg"><path d="M20.36 18"/><path d="M42 32v-4l-16-10v-11c0-1.66-1.34-3-3-3s-3 1.34-3 3v11l-16 10v4l16-5v11l-4 3v3l7-2 7 2v-3l-4-3v-11l16 5z"/><path d="M0 0h48v48h-48z" fill="none"/></svg>
                        Flight
                    </li>
                    <li>
                        <svg viewBox="0 0 48 48" xmlns="http://www.w3.org/2000/svg"><path d="M0 0h48v48h-48z" fill="none"/><path d="M14 26c3.31 0 6-2.69 6-6s-2.69-6-6-6-6 2.69-6 6 2.69 6 6 6zm24-12h-16v14h-16v-18h-4v30h4v-6h36v6h4v-18c0-4.42-3.58-8-8-8z"/></svg>
                        Hotel
                    </li>
                    <li>
                        <svg viewBox="0 0 48 48" xmlns="http://www.w3.org/2000/svg"><path d="M37.84 12.02c-.41-1.18-1.53-2.02-2.84-2.02h-22c-1.31 0-2.43.84-2.84 2.02l-4.16 11.98v16c0 1.1.9 2 2 2h2c1.11 0 2-.9 2-2v-2h24v2c0 1.1.9 2 2 2h2c1.11 0 2-.9 2-2v-16l-4.16-11.98zm-24.84 19.98c-1.66 0-3-1.34-3-3s1.34-3 3-3 3 1.34 3 3-1.34 3-3 3zm22 0c-1.66 0-3-1.34-3-3s1.34-3 3-3 3 1.34 3 3-1.34 3-3 3zm-25-10l3-9h22l3 9h-28z"/><path d="M0 0h48v48h-48z" fill="none"/></svg>
                        Rental Car
                    </li>
                </ul>
            </div>
            <div className="content">
                <div className="panel active">
                    <Flight />
                </div>
                <div className="panel">hotel</div>
                <div className="panel">rental car</div>
            </div>
        </div>
    )
}

export default TabLayout
