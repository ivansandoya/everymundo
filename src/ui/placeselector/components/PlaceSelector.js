import React from 'react'
import Autosuggest from 'react-autosuggest'
import { connect } from 'react-redux'
import { fetchRequested, clearRequested } from '../../../actions/placesuggestion'
import { places } from '../../../api'
import './placeselector.css'

function PlaceSelector (props) {
    const { input, meta, ...inputProps } = props

    let active = ''
    if(meta.active || input.value !== '')
        active = 'active'

    // Teach Autosuggest how to calculate suggestions for any given input value.
    const getSuggestions = value => {
        const inputValue = value.trim().toLowerCase()
        const inputLength = inputValue.length
    
        return inputLength === 0 ? [] : places.filter(place =>
            place.name.toLowerCase().slice(0, inputLength) === inputValue
        )
    }

    // When suggestion is clicked, Autosuggest needs to populate the input
    // based on the clicked suggestion. Teach Autosuggest how to calculate the
    // input value for every given suggestion.
    const getSuggestionValue = suggestion => {
        input.onChange(`${suggestion.name} (${suggestion.code})`)
    }

    // Render suggestions.
    const renderSuggestion = suggestion => (
        <div>
            {suggestion.name} <span>{suggestion.code}</span>
        </div>
    )

    // Autosuggest will call this function every time you need to update suggestions.
    // You already implemented this logic above, so just use it.
    const onSuggestionsFetchRequested = ({ value }) => {
        props.dispatch(fetchRequested(getSuggestions(value)))
    }

    // Autosuggest will call this function every time you need to clear suggestions.
    const onSuggestionsClearRequested = () => {
        props.dispatch(clearRequested())
    }

    const inputPropsAS = {
        value: input.value,
        onBlur: input.onBlur,
        onFocus: input.onFocus,
        onChange: input.onChange,
        className: active
    }

    return (
        <div className="placeselector" onClick={handleClick}>
            <p className={active}>{props.placeholder}</p>
            <Autosuggest
                suggestions={props.suggestions}
                onSuggestionsFetchRequested={onSuggestionsFetchRequested}
                onSuggestionsClearRequested={onSuggestionsClearRequested}
                getSuggestionValue={getSuggestionValue}
                renderSuggestion={renderSuggestion}
                inputProps={inputPropsAS}
            />
        </div>
    )
}

function handleClick(event) {
    const target = event.target
    if(target.tagName === 'DIV' && target.className === 'placeselector') {
        const autosuggest = target.lastChild
        const input = autosuggest.firstChild
        input.focus()
    }
    if(target.tagName === 'P') {
        const autosuggest = target.nextSibling
        const input = autosuggest.firstChild
        input.focus()
    }
}

function mapStateToProps(state, props) {
    return {
        suggestions: state.placesuggestion.suggestions
    }
}

export default connect(mapStateToProps)(PlaceSelector)
//export default PlaceSelector
