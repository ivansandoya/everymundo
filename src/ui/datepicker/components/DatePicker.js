import React from 'react'
import DayPickerInput from 'react-day-picker/DayPickerInput'
import { DateUtils } from 'react-day-picker'
import dateFnsFormat from 'date-fns/format'
import dateFnsParse from 'date-fns/parse'
import './datepicker.css'
import 'react-day-picker/lib/style.css'

function DatePicker (props) {
    const { input, meta, ...inputProps } = props

    let active = ''
    if(meta.active || input.value !== '')
        active = 'active'

    const handleDayChange = (day) => {
        input.onChange(day)
    }

    const FORMAT = 'YYYY-MM-DD';

    return (
        <div className="datepicker" onClick={handleClick}>
            <p className={active}>{props.placeholder}</p>
            <DayPickerInput 
                onDayChange={handleDayChange}
                formatDate={formatDate}
                format={FORMAT}
                parseDate={parseDate}
                placeholder=''
                value={input.value}
            />
        </div>
    )
}

function handleClick(event) {
    const target = event.target
    if(target.tagName === 'DIV' && target.className === 'datepicker') {
        const datepickerWrapper = target.lastChild
        const input = datepickerWrapper.firstChild
        input.focus()
    }
    if(target.tagName === 'P') {
        const datepickerWrapper = target.nextSibling
        const input = datepickerWrapper.firstChild
        input.focus()
    }
}

function parseDate(str, format, locale) {
    const parsed = dateFnsParse(str, format, { locale });
    if (DateUtils.isDate(parsed)) {
        return parsed;
    }
    return undefined;
}

function formatDate(date, format, locale) {
    return dateFnsFormat(date, format, { locale });
}

export default DatePicker
