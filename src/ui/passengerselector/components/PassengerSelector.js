import React from 'react'
import { Field } from 'redux-form'
import { connect } from 'react-redux'
import './passengerselector.css'

function NumberInput (props) {
    const { input, meta, ...inputProps } = props

    return (
        <input 
            type="number"
            step="1"
            min="0"
            max="10"
            value={input.value}
            onBlur={input.onBlur}
            onFocus={input.onFocus}
            onChange={input.onChange}
        />
    )
}

function PassengerSelector (props) {
    return (
        <div className="passengerselector" onClick={openPassengerOptions}>
            <span className="detail">{props.adults} Adults, {props.children} Children, {props.infants} Infants</span>
            <p className="total">{parseInt(props.adults) + parseInt(props.children) + parseInt(props.infants)} Passenger(s)</p>

            <div className="options" id="passengerOptions">
                <ul>
                    <li>
                        <div>
                            <p>Adults</p>
                            <span>12+ years</span>
                        </div>
                        <Field 
                            name={'adults'}
                            component={NumberInput}
                            value="1"
                        />
                    </li>
                    <li>
                        <div>
                            <p>Children</p>
                            <span>2 - 11 years</span>
                        </div>
                        <Field 
                            name={'children'}
                            component={NumberInput}
                            value="0"
                        />
                    </li>
                    <li>
                        <div>
                            <p>Infants</p>
                            <span>0 - 1 years</span>
                        </div>
                        <Field 
                            name={'infants'}
                            component={NumberInput}
                            value="1"
                        />
                    </li>
                    <li onClick={closePassengerOptions} id="close">
                        Close
                    </li>
                </ul>
            </div>
        </div>
    )
}

function openPassengerOptions (event) {
    const passengerOptions = document.getElementById('passengerOptions')
    if(event.target.id !== 'close')
        passengerOptions.style.display = 'block'
}

function closePassengerOptions (event) {
    const passengerOptions = document.getElementById('passengerOptions')
    passengerOptions.style.display = 'none'
}

function mapStateToProps(state, props) {
    return {
        adults: state.form.flight.values.adults,
        children: state.form.flight.values.children,
        infants: state.form.flight.values.infants,
    }
}

export default connect(mapStateToProps)(PassengerSelector)
