export const FETCH_REQUESTED = 'FETCH_REQUESTED'
export const CLEAR_REQUESTED = 'CLEAR_REQUESTED'

// load places suggestions to state
export const fetchRequested = suggestions => ({
    type: FETCH_REQUESTED,
    payload: suggestions
})

// clear all places suggetions
export const clearRequested = () => ({
    type: CLEAR_REQUESTED
})
