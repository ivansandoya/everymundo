import React from 'react'
import { render } from 'react-dom'
import { createStore } from 'redux'
import { Provider } from 'react-redux'
import Booking from './src/Booking'

// import reducers
import rootReducer from './src/reducers/index'

// create store
const store = createStore(
    rootReducer,
    undefined,
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
)

// frontend container
const container = document.getElementById('container')

// connect store to components using a HOC Provider
render(
    <Provider store={store}>
        <Booking />
    </Provider>, 
container)
